% Notes from CENG111 lectures of Sinan Kalkan
% Erencan Ceyhan
% 2022-02-03

# Introduction to Computer Engineering Concepts

Computer Science
: The name is somewhat misleading. The science starts way before the computers. We could **compute** things way before them. It's not about how 
we write programs either. Sometimes, implemented solutions may not even exist.

Computer science is the study of algorithms, their properties, implementations and resulting applications.

Usually, even for very simple problems like _measuring the height of a building using a barometer_, there are many different solutions with different pros 
and cons. Finding a number in sequence is a similar one for computers.

Algorithm
: The name comes from the Latinized version (Algorithmi) of the Persian mathematician, astronomer and geographer 
[al-Khwarizmi](https://en.wikipedia.org/wiki/Muhammad_ibn_Musa_al-Khwarizmi), just like the word _algebra_. He presented solutions for linear and quadratic 
equations in a **systematic** way with **steps**.

So, an algorithm is a procedure or formula for solving a problem. "_A set of instructions to be followed to solve a problem_" or "_an effective method 
expressed as a finite list of well defined instructions for calculating a function_" are also valid definitions.

Formal Definition
: "Starting from an initial state and initial input (possibly empty), the instructions describe a computation that when executed, 
will proceed through a finite number of well-defined successive states. Eventually producing **output** and terminating at a final ending state."

+ **STEP 1**: Do something
+ **STEP 2**: ...
+ **STEP 3**: ...
+ ...
+ **STEP N**: Stop.

Sequential
: Like on above, simple well defined list of usually declerative sentences.

Conditional
: Doing something _if_ some other thing is true.

Loop
: Doing the same thing until either something is not true.

Simple tasks of our life such as coming to the class, cooking, dresing up according to the weather are all algorithms actually.

**Algorithm to Wash Hair**:

1. Wet hair.
2. Set WashCount to 0.
3. Repeat step 4 to 6 until WashCount equals 2.
4. Lather your hair.
5. Rinse your hair.
6. Add 1 to the WashCount 
7. Stop, you are finished.

Algorithms expressed like above are called as **pseudocode** as they are not strict lingustically (written in human languages) so they are not **code** exactly.

[However, algorithms we use daily are usually ambigious](https://www.youtube.com/watch?v=cDA3_5982h8).

Flow Chart
: Graphical representation of code with shapes (different for e.g. conditionals and loops) and arrows. However, as this approach creates big 
drawings, in many problems pseudocode is used which is much more compact.

+ **Algorithms are the foundation of all programs**

\pagebreak

# Computation

We can divide computation types using several approaces:

+ Digital (modern audio recorders using formats like MP3/AAC/OGG etc.) vs analog (old-school casettes)
+ Sequential (Good-old CPUs) vs parallel (Multicore CPUs, almost all GPUs, most supercomputers etc.)
+ Batch (`python3 main.py`) vs interactive (`python3`, `>>> a = 5**2`)
+ Evolutionary, molecular, quantum computation
+ "Physical computation", "Digital Physics" ("God is a C programmer" etc.)

Parallel computing is usually done in clusters.

In evolutionary computing, one gives genes and using this, they can compute things. It's pretty widespread in AI fields.

In our brain, there are great amouont of neurons (55-70 millions). When a neuron sends an electric potential to the next one, if the potential is more than 
a certain treshold, the next one is activated. How they activate is calculated is called _activation function_.

We can mimic this approach with _deep learning_. However, on average, our brains have much more neurons and synapses, much more than the transistor number of 
comtemporary computers.

Von Neumann Architecture
: Describes the composition of a computing machine and how they work.

Turing Machine
: A formal definition of a computituonal device. Input comes on a tape which has symbols and the computer reads them and does the 
corresponding action for the symbol it has just read.

![Turing Machine](01_Turing_Machine.png)

\pagebreak

# Computer Components and Logic Gates

When one opens a computer, they see some components. Importantly:

+ CPU
+ Memory

These components consist of **logic gates**. Those gates consist of tiny (even on atomic level) transistors (switches that have three legs, when the current 
passes through the middle leg, the switch lets current pass between the other legs).

A computer consists of almost fully "bits", which can be either 0 or 1. This corresponds to non-existing (0V) and existing (5V) electrical current, 
respectively.

Transistors back then were pretty big. In time, the human civilization managed to shrink the transistors, making them able to fit more and more transistors 
into the same area. Increased transistors means more logic gates, which means more computational power. [However, there is a limit for this 
progression](https://en.wikipedia.org/wiki/Moore%27s_law#Forecasts_and_roadmaps).

[Click here for logic gates, their representation and their truth tables](https://en.wikipedia.org/wiki/Logic_gate#Symbols)

Please note that one can build a whole working computer using only `AND`, `OR` and `NOT` gates (actually `NAND` is sufficient enough but it may be 
considered as a "compound gate").

## Water Tank Problem

![Water Tank Problem](02_Water_Tank_Problem.png)

|H|L|Tap|Sink|
|----|-----|----|----|
|0|0|0|0|
|0|1|1|0|
|1|0|0|1|
|1|1|0|0|

Tap := `AND(NOT(H), L)`

Sink := `AND(NOT(L), H)`

\pagebreak

## Binary Addition

|Argument|S1|S2|S3|S4|S5|S6|S7|S8|
|--------|--|--|--|--|--|--|--|--|
|a|0|1|0|1|0|1|0|1|
|b|0|0|1|1|0|0|1|1|
|c~in~|0|0|0|0|1|1|1|1|
|--------|--|--|--|--|--|--|--|--|
|s|0|1|1|0|1|0|0|1|
|c~out~|0|0|0|1|0|1|1|1|

s := (a\` * b\` * c~in~) + (a\` * b * c~in~\`) + (a * b\` * c~in~\`) + (a * b * c~in~)

c~out~ := (a * b * c~in~\`) + (a * b\` * c~in~) + (a\` * b * c~in~) + (a * b * c~in~)

A binary adder can be used to construct adders for more bits. Here is an 8-bit example.

![8-bit Adder](03_8_Bit_Adder.png)

There is an efficiency drawback in this circuit: digits are calculated sequentally; thus, this circuit becomes pretty slow for more, say 64, bits.
However, some smart guys (and gals probably) improved this circuit so that digits can be predicted before they are calculated.
When we design a solution for a big problem like 64-bit adder, we **divide** it into smaller problems (binary adder) and **conquer** (combine like above).

Decimal addition and binary addition are the same except the highest digit is 1 in binary and 9 in decimal.

The size of the truth table is _2^n^_ when _n_ inputs are given. Thus, for things like 32-bit or 64-bit operations the truth table becomes _huge_. In this 
case, we use the beforementioned **divide and conquer**. Instead of directly adding two numbers, we combine the solution for the smaller problem (**full adder** in 
this case).

\pagebreak

# Architectures (Mostly von Neumann Architecture)

![von Neumann Architecture and FDE Loop](04_von_Neumann_FDE.png)

## Fetch

When a program is to be executed:
+ Its code is loaded into somewhere in the memory
+ The address of the first instruction is given to the CPU (via instruction pointer/program counter, pointing to the next executed instruction)
+ The first instruction is fetched from the address into the _instruction register (IR)_

Memory Bit
: The amount of bits in a single row of the memory. It may be 8, 16, 32, 64 or even some non-power-of-two number like 18 (especially in early 
computers) bits.

Size of the Memory
: Number of rows multiplied by memory bits. Expressions such as 8GB, 16GB are actually number of rows. There are 8 bits (or 1 byte) per 
row in those memories. (Note from me: Teacher said like this but actually 8GB is total memory size, as by chance(!) number of bits per row is the same with the 
number of bits in a **byte**.)

Size of data:

+ 1 Kilobyte = 1024 bytes (2^10^ bytes)
+ 1 Megabyte = 1024 KB (2^20^ bytes)
+ 1 Gigabyte = 1024 MB (2^30^ bytes)
+ ...

As there are more than 1 rows, when CPU requests some data (including instructions), it needs to give the information of the _address_, the number of the 
row. The size of this address is fixed, so (let bit count of address be _k_) the size of the maximum _addressable_ row amount is also fixed, _2^k^_. (Real life 
example: 32-bit computers can't access over 4GB of RAM since the bit count of address is 32, hence the maximum address is 2^32^ bytes = 4GB.)

CPU also needs a connection to specify whether it wants to _read from_ or _write into_ the memory. There is also of course a connection that carries the 
data, usually between **registers** (special short-term data store slots inside the CPU) and the memory.

## Decode

A CPU has two main parts:

+ Control Unit
: Manages the fetch-decode-execute cycle

+ ALU
: A fancy calculator

Parts of the memory subsystem:

+ Fetch/Store (Read/Write) controller
   + Fetch
   : Read data from memory
   + Store
   : Store data in memory
+ Memory address register (MAR)
+ Memory data register (MDR)

Memory is much closer to the CPU and faster to access compared to the hard drive. However, RAMs are still painfully slow compared to the CPUs. To lessen this 
problem, some smart people created even closer and faster, but much smaller and more expensive low-capacity memory called _cache_. CPUs access cache _before_ 
looking at the RAM. Usually, there are three levels of cache: **L1**, **L2** and **L3**. L1 is the fastest and smallest and L3 is the slowest and largest, while 
being still much faster than the RAM. CPU looks at them in this order. As both the speed and the capacity of caches increase, CPUs use their time less and less 
on read/write (IO) operations.

Let's add into the first part of the fetch section:

+ The sequence of 0s and 1s in the IR currently gives us the instruction (operation) and the parameters of this instruction. The format depends on the 
architecture (RISC vs CISC, fixed-length vs variable-length and of course the bit placements) but it must be consistent within a certain CPU.
+ The CPU looks for the instruction. The code inside this IR is called _opcode_. The placement of this information depends on the architecture.
+ As the opcode is decoded, now our CPU looks for the parameters. Some instructions like `NOP` (no operation) doesn't have any parameters (half-instruction) 
whereas some like `ADD` has. The decoder invokes the corresponding circuit (adding, shifting, I/O mechanisms etc.) of the CPU.

## Execute

There are not many to say:

+ The PC is increased by the number of the slots this instruction encompases (1 in our case, fixed in fixed-length formats and depends on the opcode in 
variable-length formats).
+ The corresponding mechanism (ALU, memory connections, other IO connections like disk or USB etc.) does its work according to the instruction and its 
possible paramter(s).

Instruction types:

+ Data Transfer
: Move contents of a container (register or memory slot) to another container.
+ Arithmetic/Logical Operations
: From addition and substraction to `AND`, `OR` or bit shifting.
+ Branch Instructions
: Specify which instruction will be executed next depending on the situation of the previous instruction(s).
+ Comparison Instructions
: Compare values and and store result in a certain place (usually something like a seperate `FLAGS` register).

Register types:

+ User Accessible Registers
: Most registers.

+ Data Registers
: Accumulator, MDR. Used in various operations.

+ Address Registers
: MAR, stack pointers. Holds various addresses.

+ Constant Registers
: Can't be changed. Zero Register in ARM architecture is an example.

+ Special Purpose Registers
: CPU uses these registers implicitly in some operations like memory addressing or hardware stack.

Many CPUs have different amount of registers. For example, x86-64 and ARM have 16 integer and 16 floating point registers. x86 architecture has 8 each and some architectures (especially older ones) doesn't even have floating point registers.

von Neumann Architecture
: Less complex to design compared to Harvard Architecture (as instead of seperate memories, von Neumann Architecture uses a single memory for both data and code) and has better multi-tasking ability thanks to its ability to re-locating code.

Harvard Architecture
: Instead of putting both data and instructions in a single memory, this architecture seperates these two information into two different types of memory.

**NOTE**: Even though modern hardware uses von Neumann architecture, operating systems usually treat programs like in Harvard architecture, as in many cases, seperating them yields better results in e.g. security.

SIMD (Single Instruction Multiple Data)
: Same processor (or core) executes the same instruction on different data.

+ In x86-64 architecture, CPU can handle arithmetic operations on at most 64-bits natively; however, this architecture has **SIMD** extensions utilizing which we can do the same operation (like addition or `AND`ing) on bigger numbers like 256-bit ones, but four 64-bits are treated seperately in this case. The result is the same as doing the same operation on these four 64-bit numbers seperately, but the operation is done in parallel instead of one-by-one. [Here](https://en.wikipedia.org/wiki/Single_instruction,_multiple_data) you can get more information about this method.

MIMD (Multiple Instruction Multiple Data)
: Multiple processors (or cores) execute different instructions on mostly different data.

+ Web servers are nice examples. Take Google. Two different machines (and their users) can request different search queries at the very same time

Optical Computing
: Using photons to perform calculations. Currently only available for data transfer and has many disadvantages and challanges.

Quantum Computing
: Computing using **qubits**, which has an additional **fused** state on top of the classic 0 and 1. Perform some instrucitons on _all possbile_ inputs at once. However, increased amount of qubits creates an environmental impact.

Memory Types:

+ Dynamic Memory
: Every bit is represented with high voltage (HV, 1) or low voltage (LV, 0). It's very easy to implement; however, it loses the information less than a blink of an eye when it's not electrially sustained.
   + The data also disappears with time. To prevent this, smart people designed the CPU so that it refreshes the contents constantly.
+ Static Memory
: A coupled transistor system stores the information by one trigerring the other. It's much faster than dynamic memory.
   + This memory is much more expensive compared to DRAM (Dynamic RAM), so DRAM is used much more extensively compared to SRAM (Static RAM).
+ **Volatile Memory**: The stored data is lost when the electricity is turned off. DRAM and SRAM are exapmles.
+ **Non-Volatile Memory**: Read-only memory (ROM), flash memory (electronically programmable version of ROM and used in place of ROMs in many modern systems. How do you think you can update your BIOS?).
+ **Virtual Memory**: A technique to manage memory of programs and abstraction of different memories like RAM and disk.
   + When the memory is full, OSs move the virtual memory of some less-frequently used programs into the disk. A certain partition for this use case is called **swap** on Linux. Windows uses a special file called **pagefile** to achieve this.
   + To simplify processors, they are designed to work on real memory, while a component called **MMU (Memory Management Unit)** translates the access into the right component.
   + In order to simplify all those jobs, some smart people created a chunk system called _pages_, which work on virtual memory (RAM and swap). MMU can also check like if the accessing program has access to the page in request.

Direct Memory Access (DMA)
: Some jobs like copying data from disk to the RAM are very inefficient if all the data passes through the CPU. Therefore, some smart people created DMA. When there's some big data to read, CPU invokes this unit and continues executing instructions. Meanwhile, data is transferred betweem disk and memory with DMA controller controlling it.

Peripherals
: RAM is not the only component of the CPU interacts. Through the single bus that also connects RAM and CPU, HDD and GPU (which needs _very_ fast connection, at least 30 frames per second with millions of pixels each consisting of at least 3 bytes) are conntected by this fast bus. That means CPU can use the same instructions to access these components. CPU can send a signal to this bus and the correct component can do the task. To achieve this, a certain part of the RAM is allocated for different components, so reading from and writing into this part actually does the task _inside_ another component depending on where the memory _maps_. This is called _memory map_. Please note that memory map does not cover the whole memory size.

Other peripherals like keyboard and mouse can work more than fine even with abysmally slow bus since the CPU can execute billions of instructions per second (I don't think anybody can become that fast in typing, even fastest touch-typers etc. can't reach even a fraction of this number). Hence, these components use `in` and `out` instructions [or similar instructions in non-x86(-64) architectures] for I/O. These components are isolated from the main bus, so tasks using them are called _isolated I/O_.

Mass (Secondary) Storage Devices:

+ **Direct Access**:
   + HDD, CD_ROM, DVD...
   + Uses its own address scheme to access data
+ **Sequential Access**:
   + Tape drive and similar
   + Stores data sequentially
   + Very slow compared to direct access ones; however, they are much durable and can have much more capacity (same capacity tape cartidge is about half the price of the HDD).
   + These properties of sequential access storage media makes them very suitable for data archival.

HDD
: They use literal spinning disks to store data. To read/write, a spinning head moves between sectors and tracks, and the disk turns to get into the right position for the data I/O.

+ More than one disks are usually found in these devices. They can work in parallel.
+ RPM (Revolutions per Minute) is an important factor in the speed of these disks.
+ Seeking
: Finding the correct sector and track (around 9ms)
+ Rotating disk
: Getting into the right position (depends on RPM)
+ Transferring data
: Transferring data.
+ These three steps determines the access time.
+ The first HDD (IBM 350) was _huge_ in terms of physical size and _tiny_ in terms of digital size
+ HDDs use magnetic fields to store data. The direction of the field determines the value of the bit.
   + Therefore, if one puts an HDD near a strong magnetic field, they may corrupt the data on the HDD.
+ **Average Seek Time**:
   + x: track we want to move onto
   + y: current track
   + average seek time := 1/L^2 integral(0,L, integral(0,L, |x-y|dx)dy)

![HDD Disks](05_HDD_Disks.png)

Port

: Conceptually, they reminds real ports. An external component (ship) gets near the port and exchanges data (people, crates...).
+ **SATA (Serial Advanced Technology Attachement)**: Interface between motherboard and storage devices. SATA 1, 2 and 3 has 1.5, 3 and 6 GB/s speeds, respectively.
+ **Firewire**: Alternative to USB.
+ **Universal Serial Bus (USB)**: Can transmit both data and power. 2.0 and 3.0 are widely used.
+ **Ethernet**: Used for internet and intranet. Up to 1Gbps (Gigabit per second, 1/8 of Gigabyte per second) ones are widely used, faster ones are available.

Interrupts
: CPU does the FDE cycle, but we need to transmit other inputs like keyboard presses or mouse movements. To do this, CPU may look for each IO device after each cycle, but this approach called _pulling_ is very inefficient.
Some smart people however developed the _interrupt_ mechanism. At the beginning of each cycle, the CPU looks for whether there are any interrupts (which are stored in special interrupt register which also holds the information of the type of the interrupt).
If there are any interrupts, CPU looks at the _interrupt table_, which holds the information that maps each interript with a corresponding address that points to the handling sequence.
To jump into this handler, CPU saves the current state of the program (so, registers). When the handler finishes the CPU gets back the previous state of the program.

+ We can use interrupts for from I/O and time management to the poweroff signals and traps.
+ An interrupt table is provided by the BIOS at startup, but the OS can overwrite this table.
+ A system timer generates interrupts on each "tick". They are called once every 55ms, but this is programmable. This interrupt can be used for memory refreshing and OS time management.
+ Note that in normal circumstances, we can't reach the speed of the CPU for sending interrupts, so we can't overload the CPU.
+ Some interrupts are more critical, so sometimes some interrupts are _masked_, a.k.a. ignored.
+ When we try multitasking with single CPU/core, actually the OS switches between processes in fixed amount of time. Hence, each program actually runs only a fraction of time, but it oscillates so fast that we can't notice this change with naked eye.
   + This is one of the main uses of the timer interrupt.

\pagebreak

# Booting Process

CPU must do FDE cycle, so when we press the power button, CPU fetches the BIOS. It's stored in a special part of the motherboard and loaded into the memory.
It first initiates a self-check of hardware (POST, Power-On Self Test)
If everything is okay (a single beep in some motherboards) the BIOS looks for something to boot in the storage devices. There's a changable order for this lookup.
BIOS loads the _MBR_ (Master Boot Record, an information table at the beginning of the drive that stores the partitions and their bootability) and looks for a bootable partition.

+ Last 2 bytes of MBR is 0x55AA (big-endian, in hexadecimal) and before that, MBR may include a 16 bytes * 4 region for _primary_ parititions.

UEFI is an update and improvement for the BIOS.

+ Managed by a group of chipset, hardware, system, firmware and operating sstem vendors (UEFI Forum)
+ Faster checks compared to BIOS
+ Ability to add applications (making it a tiny OS)
+ Secure boot, which disallows unauthorized programs from booting
+ A neat GUI
+ Requires GPT
   + An extension to MBR
   + _GUID_ (Globally Unique IDentifier): Creating almost unique identifier for each partitions.
   + Support for more than 2TB disks and up to 128 parititions compared to 4 of MBR.
   + A backup at the end, making it easier to execute partition recovery process.

\pagebreak

# Operating System

Hardware level is very hard to work with. We need to know the CPU like our palm, which is not user-friendly, making many people unable to use the computer. This is why the human race created the _operating systems_, they handle those hardware processes, abstracts them and provides a much better interface for any user.
All those abstracting programs are called as _system software_ collectively. BIOS and OSs are included in this concept.

OS provides:

+ Language Services
: We need them to write and execute programs:
   + Interpreters
   + Assemblers
   + Compilers
+ Memory Managers
: Even though they may look big, memory must be handled correctly since they fill out pretty quick.
   + Program Loader
   + Garbage Collectors: Say, a program crashed while occupying a portion of the memory. The GC in the OS marks this space as available when the program exits, by design or by another reason.
   + Linkers
+ Information Managers
: Although the disk is much bigger, it still needs to be handled, including permissions and backups.
   + File Systems: There may be holes in the drie. This fragmentation is handled by FS.
   + Database Systems: Storing tabular data so that we can access and modify the data quickly.
+ Schedulers
: Multi-tasking in one CPU (or multi-tasking with task amount greater than the core/processor amount). Time and all resources must be handled correcty (otherwise a [deadlock](https://en.wikipedia.org/wiki/Deadlock) happens)
+ Utilities
: Other utilities 
   + Text editors, graphics routines...

\pagebreak

# Programming

Program
: When we come across a real world problem, we need to design a solution. The solution has two parts: algorithm and data. The **implementation** of this solution is called **program**.

+ Writing something on assembly level (which has almost 1-1 correspondance with machine languages) is cumbersome and writing anything non-trivial becomes a complex monster pretty quickly. To abstract out this issue, some smart people created "high level languages" which are converted to assembly and after this these outputs are converted into native machine code.
+ In a typical assembly program, there are two parts: code and data part (doesn't this sound somewhat familiar, like, umm, Harvard architecture?). Data part is usually after a halt instruction in order to prevent execution of data.

## Programming Language Classification

: There are more than 700 low- and high-level languages. However, there are many similarities among each other. These "doctrines of programming" are called as **paradigms**.

+ Imperative
: Execution follows sequential statements. Only conditions or loops may change this order. This is pretty similar to FDE cycle and its style. One can use variables, which are **names** of data. One has direct access to data.
+ Functional
: The algorithm is decomposed into functions. Result of the execution of this function is passed to another one. The composition of all those little functions creates the soltuion. Functions are higher-level statemets.
   + One does not have direct access to the data. To mitigate this issue, many FP lamnguages implement some non-FP language constructs, which decreases the **pureness** of the language.
+ Logical-Declarative
: One declares the _rules_ between different data and the language provides _inference_ engine to create more related rules and algorithms.
Programmers can work on complex relationships with simple rules. Some languages provide query system so that a programmer can
check complex relationships within little rules.
   + Underlying paradigm is very different from imperative and functional paradigms.
+ Object Oriented
: In imperative paradigm, we seperate actions and data. In OO paradigm, instead, we combine them into packages called **objects**.
In a single object, there are both the data and related actions. **Message** is the interaction between two objects. The web of messages results in the soltuion.
+ Concurrent
: Even in a simple home computer, there are many CPU cores and much more GPU cores. In order to utilize this power, we can specify
functions that will run in parallel.
+ Event Driven
: When we click on a button, an associated _callback_ is called after a certain **event** fires. Events fire when something happens and corresponding
functions are called. Interrupts are somewhat the hardware versions of events.

Anything that can be computed can be represented in _any_ programming language
([as almost all programming languages are Turing complete](https://en.wikipedia.org/wiki/Turing_completeness)). However, some programming languages are better
in _certain situations_ compared to other languages. Like a shovel and a grader; one can use both to do any soil movement, but shovel is better in certain
circumstances like small amount of soil whereas grader is much better in moving big amount of soil. For example, to prove some theoretical thing,
declarative programming is better, finding pixels with certain intensity in an image is faster using concurrent languages and designing GUIs
is in the territory of event driven approach.

Programmer's own taste is also a driving factor in language choice. Do it in what you are the best.

Especially in business, circumstance imposed concerns like time limits may change the choice. HLL languages like Python shine here.

Current trends and the popularity of the language may affect the choice. Writing an OS in Rust is more than possible, but C is much more popular in
OS development, so resources are much more plentiful and one can have answers to their questions easier because of the size of respective communities in OS development.

Another common categorization method is how the language is executed.

+ Compiled
: A **compiler** produces **object code** (code of functions) from the source and a **linker** fills the holes in the object code with the references to the
appropiate **symbols**. Most compilers nowadays does the linking work without asking. The generated **executable code** can directly be executed by the CPU.
+ Interpreted
: An **interpreter** is a layer between the programmer and the CPU that takes the request from the programmer, interpret the request
and map it into something the CPU can understand.

When programs are written, developers almost always divide the problem into almost-independent modules (UI, DataBase, Control module...) and functions (read from DB, 
initialize the UI...). A typical program consists of usually aforementioned components. Of course there are many more types of programs. For instance, a program
may take some images from the camera and process it in a deep neural network to _understand_ or maybe even edit it. All those components can exchange information
between them, like UI buttons alerting the camera to take picture or UI getting the processed information from the network and representing it to the user.
The network then may send this information to the database module to store it in a DB.

+ Top-Down Test
: Test the whole product and then filter the problem to the modules and find the wrong module.

+ Bottom-Up Test
: Test the modules individually and then get into the top gradually.

+ Black Box Test
: Just give the input and get the output and compare this with the expected outputs. Our assignments and exams are tested like this.

+ White-Box Test
: Similar to the black box but we know the inner working of the program, so we may adjust our inputs accordingly to, say, edge cases and such.
If we fail the black-box testing in the school, teachers will switch to this in order to give better grades (our program may have the right algorithm with more
edge cases due to something and a whitebox test can observe this whereas a blackbox one can't).

Bug
: Errors of a program. The term comes from an early punch card computer. When a bug caused the machine to malfunction, those programmers gave the name
of bug to errors, which has been used since then.

Error types:

+ Syntax error
: Like "a x b" is not valid while "a * b" is valid in most programming languages. They are usually easy to catch since the compier/interpreter
complains about it before running the program.
+ Runtime error
: The syntax is fine; however, due to some values _in_ the _runtime_, the program may stop prematurely. Like dividing by zero, which is
usually caused by wrongly-formed values. These errors are more difficult to catch.
+ Logical error
: The compiler doesn't complain and it runs well but the output is wrong. Maybe we forgot some parantheses (6+4/5 does not produce 2). These
errors are even harder to find.
+ Design error
: The whole algorithm is wrong! Like using quadratic formula to find the roots of a third order equation.
+ [Heisenbug](https://en.wikipedia.org/wiki/Heisenbug)
: A bug, well, that can't be seen in a debugging process but can be seen in a normal run. These
may make you regret being a computer engineer.

\pagebreak

# Python

Python is an interactive and interpreted scripting language.

It tries to be readable. The very design of the language started with this readability.

The principles that create this property are written in the _Zen of Python_.

Python mainly support functional, object oriented and imperative paradigms.

The language was released in 80s. In 2000, Python 2 was released, which was a very major update and this verion(s) is what the big companies started to use.

Python 3 was released in 2008. This verions broke the backwards compatibility. Some features from 3 are implemented in the last versions (2.6 and 2.7) of Python 2.

The name actually does not have any relation wit hthe snake. The source was the British comedy group _Monty Python_. Guido van Rossum (the creator) got inspired
by the script of "Monty Python's Flying Circus".

\pagebreak

# Data Representation

(459)~10~ = 4*10^2 + 5*10^1 + 9*10^0

(101101)~2~ = 1*2^5 + 0*2^4 + 1*2^3 + 1*2^2 + 0*2^1 + 1*2^0 = (45)~10~

In base-10, which we use in almost anywhere, we have 10 digits (0...9). In the binary base system (base-2) we have 2 digits (0,1).

To convert a number written in a base, we substract the biggest powers of the target base. If we can't divide for a certain digit (which is one more than the power), 
the digit is zero. If we can, the result is the digit.

Another (more systematic) way to do this is dividing. We divide the results until the result is 0. The remainders are the digits.

## Negative Numbers

(5)~10~ = (0101)~2~

(-5)~10~ = ?

### Solution 1: Sign-Magnitude Notation

We define a single bit as the _sign bit_. The remaining bits will be the _magnitude_.

+ (-5)~10~ = (1101)~2~

A CPU can be designed to interpret the number as not (13)~10~ but (-5)~10~.

This solution is very inituitive; however, it creates two problems:

+ There are two zeros! For four bits, (0)~10~ = (0000)~2~ and also (-0)~10~ = (1000)~2~. The main consequence of this problem arises in comparisons.
Even though (0000)~2~ and (1000)~2~ represents the same number, they are not the same.
+ Another problem is arithmetic. In order to do operations, we need to identify the sign and do the operations accordingly. (2)~10~ + (-2)~10~ = (0)~10~,
(0010)~2~ + (1010)~2~ = (1100)~2~ = (-4)~10~

### Solution 2: Two's Complement

Positive numbers are represented the same. (5)~10~ = (0101)~2~

To represent a negative number, we need to do something:

+ Flip the bits
+ Add 1

(-5)~10~ = (0101)~2~\` + 1~2~ = (1010)~2~ + 1~2~ = (1011)~2~

(-11)~10~ = (01011)~2~\` + 1~2~ = (10101)~2~

To convert them back, we still need the sign bit (last bit). For positive numbers (sign = 0) we normally convert. If it's negative (sign = 1) we do what
we did to convert them to positive.

+ (01101)~2~ = (13)~10~
+ (100110)~2~ = -((100110)~2~\` + 1~2~) = -((011001)~2~ + 1~2~) = -(011010)~2~ = (-26)~10~

(+0)~10~ = (0000)~2~

(-0)~10~ = (0000)~2~\` + 1~2~ = (10000)~2~ however, last bit can't be represented (it's extra) by the CPU, so it's discarded, giving us (0000)~2~ = (-0)~10~

Adding two numbers is also similar. Without looking for the sign, we can add them (or substract them) and truncate the result (a.k.a. modulo) to the
size of the operands. This is exactly what CPUs do.

+ (14)~10~ + (9)~10~ mod (16)~10~ = (7)~10~ mod (16)~10~
+ (1110)~2~ + (1110)~2~ = 1(0111)~2~ = (7)~10~
+ (0010)~2~ + (1110)~2~ = 1(0000)~2~ = (0)~10~
+ (-D)~10~ mod 2^n^ = (0-D~10~) mod 2^n^ = (0-B~2~) mod 2^n^ = [(1111...11)~2~ + 1 - B~2~] mod 2^n^ = (B~2~\` + 1) mod 2^n^
+ Let's have a look at a 3-bit system and possible values.

### Solution 3: k-excess Notation

We substract a certain number (called **bias**) from the unsigned value.

Bias of `n` bits can be calculated using 2^(n-1)^ - 1.

The main strength of this notation is that negative number come _before_ the positive numbers, just like how they actually are in the integer plane.

|Binary Value|Unsigned Decimal|Sing-Magnitude|Two's Complement|k-excess (bias = 3)|
|------------|----------------|--------------|----------------|-------------------|
|(000)~2~|(0)~10~|(0)~10~|(0)~10~|(-3)~10~|
|(001)~2~|(1)~10~|(1)~10~|(1)~10~|(-2)~10~|
|(010)~2~|(2)~10~|(2)~10~|(2)~10~|(-1)~10~|
|(011)~2~|(3)~10~|(3)~10~|(3)~10~|(0)~10~|
|(100)~2~|(4)~10~|(-0)~10~|(-4)~10~|(1)~10~|
|(101)~2~|(5)~10~|(-1)~10~|(-3)~10~|(2)~10~|
|(110)~2~|(6)~10~|(-2)~10~|(-2)~10~|(3)~10~|
|(111)~2~|(7)~10~|(-3)~10~|(-1)~10~|(4)~10~|

## Fractional Numbers

We know how to convert (19)~10~ to binary but how can we represent (5.375)~10~?

+ 5.375 = 5\*10^0^ + 3\*10^-1^ + 7\*10^-2^ + 5*1\0^-3^
+ 5 is easy: it's (101)~2~
+ We _multiply_ (not divide) the fractional part with 2 until we get 0 as fraction. Whole parts are the digits
+ 0.375 \* 2 = 0.75 + 0 then the digit of 2^-1^ (first one after the decimal point) is 0
+ 0.75 \* 2 = 0.5 + 1 then the digit of 2^-2^ is 1
+ 0.5 \* 2 = 0.0 + 1 then the digit of 2^-3^ is 1
+ Thus, the result is (101.011)~2~

Fine, but how we represent the position of the dot? Well, we have two options.

### Soltution 1: Fixed Point Representation

We divide a number to two parts: one being whole part and the other being the fraction part. Sizes may vary.

|Whole|Fraction|
|-------|--------|
|16-bits|16-bits|

However, maximum and nearest-to-zero numbers we can represent are very limited. Precision stays the same but the range is small.

### Solution 2: Floating Point Representation

Let us remember scientific notation and let's utilize it in binary.

(101.011)~2~ = (1.01011)~2~ * 2^2^

Scientific notation allows the first digit of the value as 1 to 9 and not 0. Floats are not different, but as base-2 has only 1 as the sole non-zero digit, 
we can just make it _implicit_.

|Sign|Exponent|Fraction/Mantissa|
|------|--------|-----------------|
|1 bit|8 bits|23 bits|

**NOTE**: The number uses sign-magnitude representation and the exponent uses k-excess notation. The reason of using k-excess notation is fast comparison
since order is preserved.

(101.011)~2~ = 0 10000001 01011000000000000000000

(4.1)~10~ = (100.00011001100110011...)~2~

As you can see, it's infinite (similar to 1/3 in base-10). To convert to float, we just omit the digits we can't represent.

(4.1)~10~ = 0 10000001 00000110011001100110011

**NOTE**: This means there is an information (precision) loss.

There are special numbers in the standard IEEE 754 floating point representation:

+ If both exponent and fraction parts are zero, the number is zero (+- 0)
+ If exponent is all one:
+ If fraction is all zeros, it's +- infinity.
+ If fraction is _not_ all zeros, it's NaN (Not a Number).

## Text

As 'a' does not imply any number, we need to have a **table of characters**. Fortunately, there's not many tables in use.

+ **ASCII (American Standard Code for Information Interchange)**
   + It's originally a 7-bit system but extended to 8-bits later (sometimes called as _ANSI table_)
   + Suitable for English and pretty compact. ASCII table does not have any non-English letter and ANSI is pretty limited.
+ **Unicode**
   + Skillessness of ASCII for non-English symbols created many competing encodings, (if you use Windows, 
   the odds are that in 8-bit documents there are **pages** of symbols are used, which are not compatible with each other. 
   Many documents just use UTF-16 however, which is similar to UTF-8 the teacher will explain, but uses 16-bits as base characters.) 
   but only _uni_ became the standard.
   + Originally a 16-bit standard (because the creator thought that every possible symbol we need can be represented in 2^16^ = 65536 possible numbers. 
   Also, the reason why Windows uses 16 bit encodings by default. Yes, Windows started internationalization earlier than Linux.). But it was insufficiant, 
   so it became a 16-bits variable sized UTF-16 in many systems.
+ **UTF-8**
   + Actually just a Unicode representation.
   + Backwards compatible with ASCII (probably the best attribute).
   + Dominant in web (because of endianness, which we don't need to know now) and Linux (because hackers were all USA-ers).
   + After ASCII, the first byte shows the amount of bytes in the whole character (a.k.a. code point). It's variable length like UTF-16.

We also need an extra information about the size of the string.

+ **NULL Termination**: The last character is 0 (a.k.a. **NULL**). When we come across this when reading a string, we assume that the string ends there.
+ **Explicit Size**: Right before the first character, we write the size of the text, and use it however we want.
+ Different languages and libraries use different approaches. For example C uses NULL termination whereas Python uses explicit size.

## Images

An image is a two dimensional structure of three dimensional colors (red, green and blue). Some formats also accept four dimensional colors, which have an **alpha**
**channel** on top of these three, which represents the _opacity_ of the pixel. (For instance; JPEG format does not support alpha channel but PNG does, so you can't
store images with transparent backgrounds using JPEGs, but you can using PNGs.)

Some formats do not support red-green-blue order but blue-green-red order. There's little to no difference between them for computers when they work on pixels.

## Sounds

A sound is a wave. We **sample** this data in fixed intervals and use the magnitude of the wave as the value to store. This creates a sine wave but broken in parts.

## Reliability

Computers work best in **bistable** environments (distinguishing only two states). The reason is the fact that computers degrade over time and may lose ability
to distinguish different states.

Computers understand 5V as 1 and 0V as 0. The little fluctuations due to time creates almost no difference. We may use decimal system but then we will need to
distinguish between 0V and 5V in 10 different states. This creates a huge reliability problem. We either need to reduce the amount of states (base-2 anyone?) or
increase the interval, which will both increase the circuit complexity and creates electrical danger for users.

Note from student: In your free time take a look at [ternary computers](https://en.wikipedia.org/wiki/Ternary_computer) which use three states.

# Data

Data
: Information to be processed to solve a problem.

CPU is one of the dumbest beings in universe. It can only understand integers and floating point numbers. Integer support is good but as mentioned above, floating
point numbers have severe precision loss.

But we humans are smarter, right? We need to use strings, vectors or characters while solving problems. We _need_ to translate them into something a computer
can understand. Fortunately, programming languages take this burden from us.

Numbers that cannot be represented using two's powers in the size of mantissa (like 0.1) cannot be operated on exactly. If there's also even a worse number like
pi or e, there are precision losses both in decimal and binary representations.

+ This situation makes us need not to depend on exact equality for comparisons but just look for a small enough difference.
+ Due to this, we also can't have associativity (a+(b+c) may be different than (a+b)+c).
+ In order to avoid this problem, we can:
   + Switch to the integer domain whenever we can. This also improves execution speed as CPUs can work on integers much faster compared to floating point numbers.
   + If not, choose the most precise type of floats. Using a lesser precision must be justified _only_ by being very short in terms of memory capacity.
      + In C, there are `float` (32-bits), `double` (64-bits) and `long double` (128-bits or 80-bits) that can be used.
      + Python has only `float` type which corresponds to `double` of C, but some libraries like Numpy provides other options.

Python 2 has two integer types: `int` type and `long` type. `int` is 32-bits but `long` is arbitrary precision, which means it's only bounded by the memory size. 
Python 3 has only `int` which corresponds to Python 2's `long`

Python has no seperate data type for single characters unlike C (which has `char`) but `ord(character)` and `chr(ASCII_code)` can be used to convert between integer
and string representations of characters.

Python has a seperate `bool` type (which can only store `True` or `False`) but C doesn't.

Of course, single items are not sufficient for almost any non-trivial program, so there are **container data**.

Array Representation
: Items are stored sequentially. Access time is constant; however, adding elements to or removing elements from the array is expensive.

Distributed Representation
: Items are stored in the memory in seperate places. Usually, accessing them is not something like a simple array index calculation, 
but we can add and remove item(s) easily without changing the whole structure.

String
: An array of characters. There are two approaches to store the end of it as mentioned before. In Python, they are _immutable_.

Tuple
: A heterogeneous immutable data type. We can store any data (including other tuples) in a tuple. It uses offsets, so the first element is `t[0]` where `t` is 
the name of the tuple. Tuples cannot be changed as they are immutable. `t[0] = 25` is not allowed.

+ As we can put tuples inside tuples, we can call tuples as **nested data types**.

List
: Lists are very similar to tuples, they are also heterogeneous. The main differences are that we use `[]` instead of `()` to create lists and lists are _mutable_.
We can shrink and grow the list as much as our memory capacity allows. Appending an element is done using `.append(element)` method.

+ As a side note, let's talk about methods a bit. A **method** is **function** _tied to_ an **object**. An object is an _object_ (yes an object) with **data** and
**behaviour** packed. We will look at objects in details at the end of the term.
+ There is also `.insert(index_to_insert, element)` which inserts an `element` into the list at `index_to_insert`.
+ Lists are also heterogeneous, so they may consist of any type of elements, including other lists.
+ Another thing in Python lists is that they actually don't store the objects (hence the **values**) themselves, they store what's called **pointers** to these objects.
Thus, they store only **references**. This makes them much flexible than static value arrays. Thanks to this, we can create nested lists.
+ As lists are mere references, a line like `l2 = l1` means `l1` and `l2` actually are the same lists with different names (a.k.a. **aliases**). This creates
what's called the **aliasing problem**.

`in`
: Checks whether the left argument is a **direct member** of the right argument.

Variable
: A **name** that **represents** a value.

+ In Python, variables cannot be constant, values may (like tuples and strings).
+ In most languages, `=` means "change the value of the left side to the right side".
+ In Python, variable names cannot start with digits, and they may have only English letters, numbers and underscores (`_`). `$a, var$, 12a5` are not allowed.
+ In normal circumstances (which is breached in probably on some hacky experiments), Python does not allow its identifiers (`while`, `for`, `if`...) as variable names.

+ In order to prevent aliasing problem, we can copy lists.
   + `L[:]` returns a copy of the list.
   + `copy` module has **shallow** and **deep** copy functions.

      Shallow copy
      : Only the direct elements are copied. `L[:]` does this.

      Deep copy
      : All elements are copied. Containers are copied recursively.

\pagebreak

# Actions

Finally we can represent many types of data using what we have learned. However, we can't still _use_ them! We need **actions** to **process** data.

We can categorize actions into several categories:

+ Modifying data
+ Accessing data
+ Creating data

Expression
: Actions that have data as results. `3+4*5` is an example as it produces `23`.

Statement
: Actions that only does something but does not return data as result. `del L[0]` and `a = 10` are examples.

Function
: A list of statements, expressions and usually other functions that does a **certain** job.

Here is another way to categorize actions:

+ **Basic**: Actions that do not have other actions. Like `del L[0]`
+ **Compound**: Actions that have more actions inside
   + `if` statements
   + Iterations statements
   + Functions

``` {.python}
if condition:
   statement1
   statement2
   statement3
   ...
elif condition:
   statement11
   statement12
   statement13
   ...
else:
   statement21
   statement22
   statement23
```

The weird thing about Python is that those statements (`statement1`, `statement2`, `statement3`...) _have to_ be at same **indentation** level. Indentation
level is a certain amount of **whitespace** (tab or space characters) before any code. If we mix them (tabs _and_ spaces or 2 spaces with 4 spaces etc.)
the interpreter refuses to execute.

Statements above can be any other statement including other `if`s.

Expressions are sets of **operations** and operations consist of **operands** and **operators**.

## Operators and Operands

Operators can be classified as **n-ary** operators, where n is the number of their operands. Almost all arithmetic operators are **binary** ( _2-ary_ ) operators
(addition, substraction, multiplication, division, exponention) and there is also **unary** ( _1-ary_ ) minus meaning negation.

Operations have **precedence**. `3+4*5` has two possible meanings: `(3+4)*5` and `3+(4*5)`. Of course all of us are taught the operator precedence in primary school,
multiplication and division have more precedence than addition and substraction, making the latter one true one.

If two operators have the same precedence, we need to consider **associativity**. If the operator is **left-to-right associative**, the one on the left is done first,
if operator is **right-to-left associative**, the one on the right is done first.

Operators, precedence and associativity:

+ `**` (binary, right-to-left associative)
+ `+`, `-` (unary, left-to-right associative)
+ `*`, `/`, `//`, `%` (binary, left-to-right associative)
+ `+`, `-` (binary, left-to-right associative)

`[]` is also considered as an operator in Python. It takes the indexed object and the index. It's precedence is the highest among the list above and it's left-to-right
associative.

`in` and `not in` (**membership** operators) has the least precedence in the list and they are right-to-left associative.

`==`, `!=`, `<`, `<=`, `>` and `>=` are **relation** operators. They can be used on containers as well; however, comparing containers using `not ==` or `!=` is
not recomended as it's pretty implicit.

+ Used on a container, Python interpreter iterates over the container until it gets different values at same place. If they are numbers, normal comparison is applied;
if they are "characters", their ASCII codes are compared.
+ Python has operator chaining. Like in mathematics, `a < b < c` is interpreted as `(a < b) AND (b < c)` instead of `(a < b) < c` like in some languages like C.
Thus, we shouldn't rely on the first and write what we mean explicitly.

Logical operators `not`, `and` and `or` has even lesser precedence than relational and membership operators and has precedence in that order among themselves.
`not` is unary and it is right-to-left associative. Others are binary and left-to-right associative.

**Assignments** and **combined assignments** (`+=`, `-=`, `**=`, ...) are **NOT operators**. Assignments can be chained like `a = b = c`.

Assignment can be used to swap values easily like `a,b = b,a`.

Church-Rosser Propoerty
: A reduction/re-writing system has it if the set of rules always lead to the same result independent of the order of different things: precision loss (remember
`(a+b)+c` may not be equal to `a+(b+c)`), side effects (a function for example may change some mutable global value, or change alues at addresses given to them).

Short Circuit
: Some operations like `AND`, `OR` and multiplication may have "devouring value"s which can set the output into themselves regardless of the other inputs.
`x AND 0` is always `0`, `x OR 1` is always `1` and `x * 0` is always `0`, so most langauges implement a "short circuit" system for chained such operators.
`a AND b AND c ... AND z` is evaluated into 0 if `a` is 0, and other arguments are not even evaluated.

## Notations

### Infix Notation

OP~1~ operator OP~2~

`3 + 4 * 5`

Easier to read for most humans since mathematical notation evolved with Indo-European languages, which mostly use infix notation-like SVO structure.

### Prefix Notation

operator OP~1~ OP~2~

Many functional programming languages and function syntax of mathematics use this notation.

### Postfix Notation

OP~1~ OP~2~ operator

Much similar to machine code compared to other notations, but has very limited usage among mathematics community. 

#### Postfix Evaluation

Let `3 4 5 * +` be our input. We start from the left. Let's define a **stack** which we can only push on top and pop from top.

+ `push 3`
+ `push 4`
+ `push 5`
+ `pop 2` (we now have `4` and `5` in our hands. Current operation is `*`, so we multiply them and get `20`, and then push it again to the stack)
+ `pop 2` (same as above but this time inputs are `3` and `20`, and operation is `+`, so we get `23` and push it to the stack)

This way, we get the correct result, which is `23`.

Dijkstra has invented an algorithm to convert infix notation into postfix notation. He named it as **shunting yard algorithm** since it looks like train shunting.

Let `3 * 4 / 5 - 6` be our input. We need to get `3 4 * 5 / 6 -` as output.

+ All data goes directly to the output.
+ `3` goes to output.
+ As stack is empty, `*` goes to stack.
+ `4` goes to output.
+ We have `/` now. But we move `*` on top of the stack to the output since `*` and `/` are left-to-right associative, the one on the left (the first one)
has higher priority.
+ As stack is empty, `/` goes to stack.
+ `5` goes to output.
+ We have `-`. It has lower precedence than `/`, which is on top of the stack, so we move `/` to the output.
+ As stack is empty, `-` goes to stack.
+ `6` goes to output.
+ As we consumed the output, we pop everything from the stack one-by-one and push them into the output one-by-one. As we have only `-`, only it goes to the output.

Let `A - B ^ C ^ D * ( E - ( F - G - H ) ) / K` be our input.

+ `A` goes to output.
+ As stack is empty, `-` goes to stack.
+ `B` goes to output.
+ `^` goes to stack since the operator at the top, `-`, has lower precedence.
+ `C` goes to output.
+ `^` goes to stack (as `^` is right-to-left associative, last one has the highest precedence)
+ `D` goes to output.
+ We have `*`. It has lower precedence than `^`, the operator on top of the stack, so we move `^` to the output.
+ Same as above since the top of the stack is still `^`.
+ Now we have `*` and `-` is on top of the stack. As `*` has higher precedence, we push it onto the stack.
+ `(` goes to stack (always)
+ `E` goes to output.
+ `-` goes to stack (`(` has lower precedence than any operator).
+ `F` goes to output.
+ `-` goes to stack. (`(` is on top)
+ `G` goes to output.
+ We have `-` and `-` is at the top. The `-` on the stack goes to output because of left-to-right associativity.
+ `H` goes to output.
+ As we got `)`, we pop the stack one-by-one until we get `(`. Only `-` in this case.
+ Same as above again.
+ `*` goes to output (left-to-right associativity).
+ `/` goes to stack (`/` has higher precedence than `-`).
+ `K` goes to output.
+ We pop the remaining. `/` and then `-` are moved into the output.
+ We get `A B C ^ ^ D E F G - H - - * K / -`

## Input/Output (I/O)

`print`
: Used to print text using teletypewriter. Of course this was back in 60s or so, we now have screens instead.

+ `print("My name is %s. I'm %d years old" % ("Erencan", 19))`
+ `print("My name is {0}. I'm {1} years old".format("Erencan", 19))`
+ `print("My name is {name}. I'm {age} years old".format(name="Erencan", age=19))`
+ `print(f"My name is {name}. I'm {age} years old")`
   + In f-strings like this, the Python interpreter interprets the expression _inside_ the curly braces, so we need to have `name` and `age`
   defined with appropiate values.

Programs become complext _very_ quickly. Also, many statements and expressions are calculated again and again. Therefore, we use **functions** to avoid
these problems. Instead of repeating an innamed block for numerous times, we give it a name and call it wherever we need it. Thanks to functions,
we can make our programs:
+ structured
+ readable
+ maintainable

Maintainability part shines when, say, we want to replace a little algorithm with a more efficient one in a huge code. We can just change the function
and voila, we are done.

## Functions in Python

`def` is the keyword to define a function in Python. After that, the name comes, which follows variable naming conventions. After that, in parantheses,
the parameter list should be there. The names of these parameters have to obey variable naming conventions. There must be also a following column (`:`).
The statements of a function must be identically indented. If a function supplies a value to the expression where it's called, it should have
`return exp` where `exp` is the expression whose value should be inserted.

``` {.python}
a = f(...) + 4 # becomes exp + 4
def f(a,b,c,...):
   statement1
   statement2
   ...
   return exp # the exp above is this exp
```

``` {.python}
def f(a):
   b = a**2
   return b

c = f(-3) * 4 + 2 # becomes 38
```

Functions don't have to return something, but such a function returns `None` implicitly, which is a special value and type meaning lack of data.

Functions in mathematics and programming have similar and non-similar attributes:

|Mathematics|Programming|
|-----------|-----------|
|Takes at least one parameter|Parameters are optional|
|Must return values| _May_ return values|
|Don't have side effects|Usually has side effects|

```
define f(x):
   x[0] <- "Jeannie"
   x <- ["Suzy", "Mary", "Daisy"]
   return x

s <- ["Bob", "Arthur"]
print f(s)
print s
```

The result depends on the calling method of the language.

### Calling Methods

Call by Value
: The data passed to the function is _copied_, so the code above gives `["Suzy", "Mary", "Daisy"]` and `["Bob", "Arthur"]`.

Call by Reference
: Not only the data, but also the variable itself is shared. Parameters behave the same as how they are called. The code above gives `["Suzy", "Mary", "Daisy"]` and
`["Suzy", "Mary", "Daisy"]`.

Call by Sharing
: The _reference_ is copied, but the contents are _shared_. Parameters behave like independent variables but operations _on_ their _referenced_ data is visible
to the caller function. The code above gives `["Suzy", "Mary", "Daisy"]` and `["Jeannie", "Arthur"]`.

However, if the data is immutable (numbers, tuples, strings etc.) the program behaves like **call by value** and even though aliasing at function call still
happens, the value in the memory cannot be changed.

If we want to modify such a variable, we may:

+ use global variables, which we can get using `global a` where `a` is the name of a global variable.
+ put such a value inside a list and modify using it. We can modify this list and the caller can see the effects.

### Scope

Scope
: Each function creates a scope. Functions may access any variable that has been created in their scope.

+ Local scope
: Scope that the function created itself.

+ Enclosing scope
: Scope of the function that another function is created. The scope of the outer function is an enclosing scope in nested functions.

+ Global scope
: Scope available to all functions.

+ Built-in scope
: Scope accessable from every function with the builtin functions provided by Python.

+ When accessing a variable, Python looks for this order. For modifying however Python uses local scope by default. In order to modify a variable
from global scope, we need to mark it as `global`. For enclosing scope variables, this mark becomes `nonlocal`.

However, nested functions are not advised in Python as another function may need it and scoping may create weird errors.

### Default Values

In Python, we may specify **default value(s)** in function parameters. This may be useful when we wnat to make our function usable without specifying
everything, yet retain the ability to specify them as arguments.
``` {.python}
def f(p1, p2, p3 = 10, p4 = 20):
   pass # Do something here instead of pass

f(1,2) # Valid
f(1,2,3) # Also valid
```
### Functional Programming Tools in Python

First, there is something called **higher order function**. These functions take _another function_ as their argument and use it within themselves. This is
one of the core concepts of functional programming. Python supports some nice higher order functions like `map(func, iterable)` (which applies `func` to every
item on the `iterable` and returns a new `map object`), `filter(func, iterable)` (which returns a new iterable which only consists of objects in `iterable`
that returns `True` when `func` is called on them) and `reduce(func, iterable)` (which stores a "cumulative" value and calls the `func` using this
value and the element in use and sets the result to the new "cumulative" variable).

**NOTE**: To use `reduce` in Python 3, one needs to import it from `functools` library like `from functools import reduce`.

Lambda expression
: Naming every function is pretty cumbersome if some functions are used once and are short to make naming unworthy. Sometimes those little functions can
pollute the scopes, so we have little one-line function creators called **lambda expressions**. `lambda x: x**x` returns x^x^ and takes x as its sole argument.

List comprehension
: Maths has {x^2^ \| 0 &lt; x &lt;= 2 } which is called **set comprehension**. Python has it, too but the main actor/actress/player here is **list comprehension**.
`[x*x for x in [1,2,3,4]]` expression returns `[1,4,9,16]`.

\pagebreak

# Repetation

Not all problems can be solved using simple actions we have learnt. We usually need repetation. There are two main approaches to solve such **bulky** problems:
**recursion** and **iteration**

## Recursion

Recursion
: Best way to teach this concept is probably giving some examples like factorial.

N! = 1 \* 2 \* 3 \* ... \* (N-1) \* N (recurring relations)

0! = 1 (base case)

Implementing recursion is pretty straightforward in many programming languages.

``` {.python}
def factorial(N):
   if N == 0: # base case
      return 1
   else: # recurring relations
      return N * factorial(N-1)
```
One may think that "How does a function call itself without finishing its own definition?"

There are the problems of "Which function we are in?" and "Where will we return when we finish this function?". They are solved using a **call stack**. At first,
it's empty. When we call a function, parameters and (more importantly) the state of CPU including the return address are pushed. When executing another function in a function, we keep the stack
and push the parameters and the state for this second call. When we are returning from a function, we just remove the data on the top, which is the previous state
of the CPU. After that, we jump to the address we recorded in the stack (right after the call instruction of the caller function). Some calling schemes pushes the
return value, too.

+ NOTE from student: We will probably be taught about this in the following term in C lectures. As teaching an English-esque language is probably more important
for the schools than the core concepts of computers, we will fill most of the holes we currently have in those lessons. Be patient.

When we call a recursive function, we apply the same procedure. Each "self-call" is done as if we are calling a seperate function.

+ Another NOTE: Actually, functions are nothing but addresses of the first instructions, so as the address of each function is fixed (actually not fixed but
let's don't make our brains soup), we can just "call" (actually "go to" or the infamous instruction `goto`) these addresses without knowing that it's the
address of the current function. Remember that functions are mere abstractions we mortals created to manage complex programs. For computers, there are no functions.

We need to be aware of some things:

+ We made the problem smaller (a.k.a. _divided_)
   + How this is achieved depends on the problem. Sometimes it's decreasing 1, sometimes some black magic.
+ If we don't provide the **base case** or if it's wrong for the function, the function won't stop calling itself.
   + This results in, umm, [Stack Overflow](https://stackoverflow.com/)
+ We need to combine the result with the current state ("recurring relations" part)
+ If we are not careful when dividing the problem, the result of the recursive call may be wrong, which results in incorrect result at best and computer crash
(especially when we write OS-level software) at worst.

**Examples**:

``` {.python}
def head(L):
   return L[0]
def tail(L):
   return L[1:]
def last(L):
   return L[-1]
```

Let's create a function to reverse a container.

``` {.python}
def srever(L):
   if len(L) <= 1:
      return L
   else:
      return srever(tail(L)) + head(L)
```

A function to check whether a string is a palindrome. (some text written the same when it's reversed)

``` {.python}
def isPalindrome(S):
   if len(S) <= 1:
      return True
   else:
      return head(S) == last(S) and isPalindrome(S[1:-1])
```

A function to search for a query sequentially.

``` {.python}
def sequentialSearch(query, L):
   if len(L) == 0:
      return False
   if head(L) == query:
      return True
   else:
      return sequentialSearch(query, tail(L))
```

Please note that this function is pretty inefficient. We can improve this by assuming the incoming list is sorted and using **binary search**.

``` {.python}
def binarySearch(query, L):
   if len(L) == 0:
      return False
   middle = len(L) // 2# Integer/floor division.
   # After division, the data after the decimal
   # point is discarded
   if query == L[middle]:
      return True
   if query < L[middle]:
      return binarySearch(query, L[:middle])
   else:
      return binarySearch(query, L[middle+1:])
```

A function to insert into an ordered collection.

``` {.python}
def insert(x, L):
   if len(L) == 0:
      return [x]
   if x < head(L):
      return [x] + L
   else:
      return [head(L)] + insert(x, tail(L))
```

Now we can implement a simple sorting algortihm called **insertion sort**.

``` {.python}
def insertionSort(L):
   if len(L) <= 1:
      return L
   return insert(head(L), insertionSort(tail(L)))
```

Fibonacci!

``` {.python}
def fibonacci(n):
   if n == 0 or n == 1:
      return n
   return fibonacci(n-1) + fibonacci(n-2)
```

Note that this calculates most of them for several times. If `n` is 5:

+ `fibonacci(4)` is calculated once
+ `fibonacci(3)` is calculated twice
+ `fibonacci(2)` is calculated thrice

And this grows **exponentially** depending on `n`. We can save the previous calculations and use them.

``` {.python}
def fibHelper(n, results):
   if results[n] < 0:
      results[n] = fibHelper(n-1, results) + fibHelper(n-2, results)
   return results[n]

def fibonacci(n):
   results = [0,1] + [-1] * (n-1)
   return fibHelper(n, results)
```

Here is an even better implementation that does not store all fibonacci numbers until `n`th.
Note that this also calculates numbers from the start instead of the approach we have in previous two examples, from the end.

``` {.python}
def fibHelper(i, n, fibPrev, fib2Prev):
   if i == n:
      return fibPrev + fib2Prev
   return fibHelper(i+1, n, fibPrev+fib2Prev, fibPrev)

def fibonacci(n):
   if n == 0 or n == 1:
      return n
   return fibHelper(2, n, 1, 0)
```

Here is a similar improvement for factorials.

Normal one:

``` {.python}
def factorial(n):
   if n == 0:
      return 1
   return n * factorial(n-1)
```

Improved one:

``` {.python}
def factorial2(n):
   return fact2Helper(n, 1)
def fact2Helper(n, r):
   if n == 0:
      return r
   return fact2Helper(n-1, n*r)
```

Those "improved" ones actually use something called **tail recursion**.

Tail recursion
: A situation in which recursive functions call themselves, but does not process the result and return the value directly.

Tail recursion algorithms can easily be converted into iteration. Furthermore, some compilers (GCC but sometimes) or interpreters
(Lua or LISP) may replace the last "tail" call with simple `goto` instructions, making the recursive function as fast as iterative ones.

``` {.python}
def factorial2(n):            # 1
   return fact2Helper(n, 1)   # 2
def fact2Helper(n, r):        # 3
   if n == 0:                 # 4
      return r                # 5
   return fact2Helper(        # 6
      n-1,                    # 7
      n*r                     # 8
   )                          # 9
```

``` {.python}
r = 1                         # 2
while n != 0:                 # 4
   r = n*r                    # 8
   n = n - 1                  # 7
```

## Iteration

Iteration
: We execute a certain block of statements until some certain condition is not true anymore.

``` {.python}
while <condition>:   # If this is true;
   statement1        # this is executed, else
   statement2
   statement3
   ...
   statementn
statement            # this is executed
```

Loop `while` condition is true

``` {.python}
for <var> in <iterable>:
   statement1
   statement2
   ...
   statementn
statement
```

Loop `for` each `var` in `container`.

**Examples**:

Sequential search

``` {.python}
def sequentialSearchWhile(x, L):
   i, length = 0, len(L)
   while i < length:
      if L[i] == x:
         return True
      i += 1
   return False

def sequentialSearchFor(x, L):
   for y in L:
      if y == x:
         return True
   return False
```

`break`
: If the code reaches this, the _innermost_ loops is terminated.

`continue`
: If the code reaches this, the _next_ iteration is started immediately

`pass`
: Do nothing. In some places like function blocks, Python does not allow total emptiness, so we need such a keyword.

Bubble sort:

``` {.python}
def bubbleSort(L):
   isSorted = False
   while not isSorted:
      i, isSorted = 0, True
      while i < len(L) - 1:
         if L[i] > L[i+1]:
            L[i], L[i+1] = L[i+1], L[i] # swap
         i += 1
```

Selection sort:

``` {.python}
def selectionSort(L):
   i = 0
   while i < len(L) - 1:
      j = L.index(min(L[i:]))
      L[i], L[j] = L[j], L[i]
      i += 1
```

Pancake sort: <!-- TODO: Implement this -->

Count sort:

``` {.python}
def countSort(A):
   C = [0] * max(A)
   for x in A:
      C[x-1] += 1
   i = 1
   while i < len(C):
      c[i] = C[i] + C[i-1]
      i += 1
   B = [0] * len(A)
   for a in A:
      B[C[a] - 1] = a
      C[a] -= 1
```

Iterative and recursive are the same in terms of computation capability. If one can solve a problem using one, they can solve the problem using the other
without any capability loss

If we can't convert a recursive solution into tail recursion we can still solve the problem using explicit stack and iteration.

Iteration without stack is the best one resource-wise. It uses memory least as it does not store a stackc and its flow is natural for the CPU.

Iteration with stack comes after. It uses an explicit stack (more memory and less CPU performance) but still not bad.

Recursion is hard for the CPU. It uses an implicit stack, saves even more data into this stack (CPU state etc.) and function calls are pretty expensive
in terms of CPU power.

However, recursion is pretty intuitive in many cases. Mathematician magic can easily be expressed using it.

Iteration without stack is also pretty nice; on the other hand, iteration with stack is bad in many cases as handling the stack is directly the
responsibility of the programmer.

\pagebreak

# Comparing Algorithms

One way is implementing. We can implement them on the same machine with the same language and data, and measure various usage statistics like memory and CPU usage.
This approach may work when we are comparing simple algorithms like sorting ones, but it becomes extremely difficult to implement complex algorithms.

We can count main number of steps and describe their bounds.

```
i = 0                1 * c1
sum = 1              1 * c2
while i < n          (n+1) * c3
   sum += i          n * c4
   i += 1            n * c5
print(sum)           1 * c6
```

T(n) = c~1~ + c~2~ + n \* (c~3~ + c~4~ + c~5~) + c~6

T := running time function

T(n) = a \* n + b

As all those c~1...6~ constants are machine and even time dependent, describing algorithms as them is not logical. So, we look at "bounds" when n is large.
These bounds can be expressed pretty easily.

To describe these boundaries, we have different notations.

### Big-Oh Notation (O(...))

O(...)
: A function f(n) is the running time function of a certain algorithm. f(n) = O(g(n)) &lt;=&gt; \|g(n)\| where there exists a c &gt; 0 and it's true
for every n &gt;= n~0~ where n~0~ is a large value of n

**Example**:

f(n) = 2n^2^+ 3n =&gt; O(n^2^) ?

2n^2^ + 3n &lt;= cn^2^

2n^2^ + 3n &lt; 2n^2^ + 3n^2^ = 5n^2^

2n^2^+ 3n &lt; cn^2^ &lt; cn^3^ =&gt; O(n^3^) is also true

2n^2^ + 3n &lt;= cn means O(n) which is wrong

O(n log n), O(log n)... are all wrong.

O(n^2^), O(n^2^ log n), O(n^3^)... are all true.

### Big-Omega Notation (Ω(...))

Ω(...)
: f(n) = Ω(g(n)) &lt;=&gt; c\|g(n)\| &lt; \|f(n)\| where there exists a c &gt; 0 and it's true for every n &gt; n~0~ where n~0~ is a large value of n

**Example**:

f(n) = 2n^2^ + 3n + ...

|Big-Oh|Big-Oh validity|Big-Omega|Big-Omega validity|
|------|---------------|---------|------------------|
|O(n^3^)|true|Ω(n^3^)|false|
|O(n^2^)|true|Ω(n^2^)|true|
|O(n log n)|false|Ω(n log n)|true|
|O(n)|false|Ω(n)|true|
|O(log n)|false|Ω(log n)|true|

### Big-Theta Notation (Θ(...))

Θ(...)
: f(n) = Θ(g(n)) &lt;=&gt; c~1~\|g(n)\| &lt;= \|f(n)\| &lt;= c~2~\|g(n)\| where c~1~, c~2~ &gt; 0 and it's true for every n &gt;= n~0~ where n~0~ is a large
value of n.

+ There are infinitely many upper- and lower-bounds. We use the intersection of them as the function's complexity.
+ Above example, f(n) = 2n^2^ + 3n + ... is Θ(n^2^). Θ(n^3^) or Θ(n log(n)) cannot be true unlike O() and Ω() notations, respectively.
+ f(n) = Θ(g(n)) =&gt; f(n) is both O(g(n)) AND Ω(g(n)).
+ f(n) is O(g(n)) &lt;=&gt; g(n) is Ω(f(n))
+ O(f(n)) + O(g(n)) = O(f(n) + g(n))

**Example**:

O(n^2^) + O(n) + O(log n) = O(n^2^ + n + log n) = O(n^2^)

The fastest growing one (n^2^ here) **dominates** the expression.

+ O(c\*g(n)) = O(g(n)) (constant in the definition can cover it)
+ O(g(n)) = f(n) notation (equals) is also used but mathematicians usually say "f(n) is bounded by g(n)" or such.

Best Case
: As some algorithms may behave differently depending on the input, there may have **best**, **average** and **worst** cases. For example, sequential
search has O(1) best case as the query may be the first element, instantly returning the result.

Worst Case
: The complexity of the function if it executes maximum of its instructions. O(n) for sequential search.

Average Case
: This depends on the distribition of the inputs. Even we can assume that query is in the first $\frac{1}{10}$ of the inputs we still have O(n) in sequential search.

|Algorithm|Best|Average|Worst|
|---------|----|-------|-----|
|Sequential Search|O(1)|O(n)|O(n)|
|Binary Search|O(1)|O(log n)|O(log n)|
|Bubble Sort|O(n)~1~|O(n^2^)|O(n^2^)~2~|
|Count Sort|O(n+k)~3~|O(n+k)|O(n+k)|

~1~: When the array is already sorted

~2~: When the array is reverse sorted

~3~: k is the range (difference of min and max) of the array

\pagebreak

# Structured Data

## Stack

Stack
: A Last In First Out (LIFO) data structure

Data can be added and removed from a single place.

It's an **abstract data type** as it's language-independent.

`push()`
: Puts an item onto the top of the stack

`pop()`
: Removes the item from the top and optionally returns it.

`top()`
: Returns the top element but does not modify the stack

`isEmpty()`
: Checks whether the stack is empty or not.

## Queue

Queue
: A First In First Out (FIFO) data structure.

It holds two position information: front and end.

Adding elements is done by appending on end (enqueue).

Removing elements is done by removing from the front (dequeue).

Bank/cafetaria/bus queues are everyday examples.

Sharing few resources with many demanders (a single core CPU with many running applications, for instance) is where a use case of queue.

`enqueue()`
: Puts an item onto the end.

`dequeue()`
: Gets the item from the front and steps everything towards the front by one.

`front()`
: Gets the item from the front but does not modify the queue

`isEmpty()`
: Checks whether the queue is empty.

## Priority Queue

Priority Queue
: A queue with priorities attached to each item

While storing, we may:

+ Keep the queue always ordered with respect to priorities.
   + While inserting, we can insert at the suitable point that does not change the order.
   + Inserting an element is not O(1); insertion is O(n) or O(log n) to find the place and all the movement and allocation results in O(n)
   + Dequeueing is O(1) as we know exactly where to dequeue, which is the one with highest priority.
+ Keep the queue unordered.
   + Insertion is O(1) (we just enqueue)
   + Dequeueing is O(n) as we need to search for the highest priority element.

Mathematical representation:

`createPQ() : ø`

`insert(x, PQ) : x || PQ`

`highest(x || ø) : x`

`highest(x || PQ) : x of priority(x) > priority(highest(PQ)) else highest(PQ)`

`deleteHighest(x || ø) : ø`

`deleteHighest(x || PQ) : PQ if priority(x) > priority(highest(PQ)) else x || deleteHighest(PQ)`

## Tree

Tree
: Unlike real life trees, root is usually at the top.

![Tree](06_Tree.png)

Node
: An element in a tree, including the root.

An example of tree is possible moves in a game like, say, tic-tac-toe. In ttt, after n possible moves, next player has (n-1) possible moves and so on.
This continues until no possible move remains. The program can look at the end nodes and determine the outcomes.

Level/Depth/Height
: Level/depth and height are the opposite of each other; level/depth is started to be counted from top/root and height is tarted to be counted from bottom.

Leaf/Terminal Node
: Nodes without any children. In the tree below, E, F and G are leaf nodes.

Edge
: A connection between nodes.

Branch
: Children of a node. As trees can go forever, each branch is also a tree. In a general tree, there is no restrictions but in **n-ary** trees, a
node cannot have more than n branches. The biggest example is a **binary tree**.

Binary tree
: A tree whose nodes can have _at most_ two branches. Branch are named as **left branch** and **right branch**.

![Binary Tree](07_Binary_Tree.png)

``` {.python}
[3, [10, [], [18, [], []]], [4, [], []]]
```
### Dictionary

Dictionary
: Not a data structure we will be taught, but a Python data type for a hash table (implementations of hash tables are not our topic for a while). In a
dictionary, we can use most values as **keys** and any value as **values**. `D = {"name": "Erencan", "age": 19, "city": "Ankara"}` is a dictionary with
keys `"name"`, `"age"` and `"city"` and values `"Erencan"`, `19` and `"Ankara"` respectively. `D["name"]` returns `"Erencan"`, `D["uni"] = "ODTÜ"` creates
a new key `"uni"` and sets its value as `"ODTÜ"`.

+ The reason we are taught dicts now is that instead of "0 is datum, 1 is left and 2 is right branches" approach of lists, we can use dicts for better (but
slower) tree experience.

``` {.python}
{"datum": 3,
   "left": {"datum": 10,
      "left":{},
      "right": {"datum": 18,
         "right": {},
            "left": {}}},
   "right": {"datum": 4,
      "left": {},
      "right": {}}}
```

``` {.python}
def datum(T):
   return T[0] # T["datum"]
def left(T):
   return T[1] # T["left"]
def right(T):
   return T[2] # T["right"]
def isEmpty(T):
   return len(T) == 0
def createNode(x):
   return [x, [], []] # {"datum": x, "left": {}, "right": x}
def treeHeight(T):
   if isEmpty(T):
      return 0
   return max(treeHeight(left(T)), treeHeight(right(T))) + 1
```

### Traversing a Tree

Traversal
: Visiting each node in a tree.

Let's have a tree like this:

``` {.python}
["+", [4,[],[]], ["*", [5,[],[]], [6,[],[]]]]
```

+ Pre-order traversal
: Do the job, visit left branches recursively, visit right branches recursively.

``` {.python}
def preorder(T):
   if isEmpty(T):
      return
   print(datum(T), end="")
   preorder(left(T))
   preorder(right(T))
```

Writes `+ 4 * 5 6` (prefix notation)

+ In-order traversal
: Visit left branches recursively, do the job, visit right branches recursively.

``` {.python}
def inorder(T):
   if isEmpty(T):
      return
   inorder(left(T))
   print(datum(T), end="")
   inorder(right(T))
```

Writes `4 + 5 * 6` (infix notation)

+ Post-order traversal
: Visit left branches recursively, visit right branches recursively, do the job.

``` {.python}
def postorder(T):
   if isEmpty(T):
      return
   postorder(left(T))
   postorder(right(T))
   print(datum(T), end="")
```

Writes `4 5 6 * +` (postfix notation)

### Binary Search Tree

Binary Search Tree
: Nodes on left are all smaller than the parent and nodes on right are all larger than the parent.

+ When we search for an item in such a tree, we can visit nodes pretty similar to how we binary search in an array

``` {.python}
def bstSearch(x, T):
   if x == datum(T):
      return True
   if x > datum(T):
      return bstSearch(x, right(T))
   if x < datum(T):
      return bstSearch(x, left(T))

def bstInsert(x, T):
   if isEmpty(T):
      T.extend(createNode(x))
   if x == datum(T):
      return
   if x > datum(T):
      bstInsert(x, right(T))
   if x < datum(T):
      bstInsert(x, left(T))
```

For a tree to be balanced it must be:

+ (height(left) - height(right)) &lt; 1 AND
+ left and right branches are balanced

``` {.python}
def isBalanced(T):
   if isEmpty(T):
      return True
   if abs(treeHeight(left(T)) - treeHeight(T)) <= 1:
      return isBalanced(left(T)) and isBalanced(right(T))
   else:
      return False
```

## Heap

Heap
: An array representation of a tree

Let our tree be this:

``` {.python}
[A, [B, [D,[],[]], []], [C, [E,[],[]], [F,[],[]]]]
```

We can represent it like this:

``` {.python}
[A, B, C, D, None, E, F]
#1, 2, 3, 4, 5,    6, 7
```

For an i^th^ element:

+ first branch is (n\*i)^th^ element
+ second branch is (n\*i + 1)^th^ element
+ third branch is (n\*i + 2)^th^ element
+ ...

For an i^th^ element, parent is $\frac{i - (i \% n)}{n}$ ^th^ element

\pagebreak

# Object Oriented Programming

There are **objects** which consist of both **data** and **actions**.

For instance, in games; agents have **properties** like health and skills and they have actions like jumping or attaching. This way, we **encapsulate** data and actions.

In graphical user interface programming, there are many different widgets like buttons, text fields or check boxes. These widgets share certain data aand actions
like position, size; drawing, moving, resizing. We can have these shared qualities and quantities in a **parent**, say `Widget`. These widgets **inherit** from `Widget`.

In an OO program, the logic is done by **passing messages** between objects. This way, an object doesn't need to know how another one is implemented. Only the
**interface** between objects is agreed on. This situation is tied to encapsulation.

In OOP, we can also hdie data from outside world. When these are combined, a piece of software becomes **modular** and **maintainable**.

In statically typed languages, **polymorphism** is pretty important. With polymorphism, subclasses of a certain superclass can behave as their superclass.

``` {.python}
class ClassName: # A blueprint of an object
   a = 10 # Member variable
   b = "Ali" # Member variable
   def test(self): # self is the current _instance_, test is a member function
      print("Hello!")
```

Please note that `ClassName` here is not an object, but the instances instantiated from `ClassName` _are_ objects.

In Python, every value is actually an object. Yes, even an `int` is also a class.

When we "call" a class, it returns an instance of this class.

In member functions, there must be a parameter for the current object. We can name it as anything we want (inside the rules of variable naming scheme) but in Python,
it's traditionally called as `self`. While calling a member method, we don't have to pass the current object, as the interpreter handles this implicitly.

We can use the `self` to access the current state of the object.

`__init__(self)` is a special function that is called when constructing an object, with arguments passed to the "class call".

Accessor
: Instead of exposing member variables directly, we can provide **getters** and **setters** that change or access the value with any additional actions
we want to take. This way, we can protect our objects from corruption. Furthermore, we can _prohibit_ accessing certain member variables. In Python, this is done
by adding an `_` at the start of the name. This however just "suggests" that one shouldn't access it directly. If we add `__` at the start of the name, Python
throws an error when we try to access this variable, saying that the object does not have this member (directly setting would be creation of member in the internal
dictionary of the instance). Actually, Python just changes the name to `_ClassName__memberName` like `_Person__name`. (**name mangling**)

In class definition, after the `ClassName`, we can add **parent(s)** of this class in parentheses (like `class ClassName(ParentClass):`). The `super()` function
returns this parent class so that we can use the members of the parent(s), mainly for initialization but we are free to do whatever we want actually, somewhat.

By default, Python compares the memory addresses of objects. Then, say, how two strings compare equal when their contents are the same when `str` is a class?
The answer us `__eq__(self, other)` member function. This function is called when we try to compare two objects of this class. Left side is `self` and right side
is `other`.
